# Music Box
### Description

Music Box is a web service for listening audio tracks using REST API of web server.

### Tools

* Maven
* Spring Boot
* Spring MVC
* Spring Data JPA
* Hibernate
